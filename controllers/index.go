package controllers

import "gitlab.com/rashedulAlamMazumder/go-web-app/views"

func NewIndex() *Index {
	return &Index{
		Homepage: views.NewView("bootstrap", "index/homepage"),
	}
}

type Index struct {
	Homepage *views.View
}
